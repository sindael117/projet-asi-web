--Script insertion d'auteur

INSERT INTO auteur (nom, prenom)
 VALUES
 ('Verne', 'Jules'),
 ('King', 'Stephen'),
 ('Cristie', 'Agatha'),
 ('Levy', 'Marc'),
 ('Hugo', 'Victor');
 
 --Script insertion de livre
 
 INSERT INTO livre (titre, notice)
 VALUES
 ('Voyage au centre de la Terre', 'Jules Verne, Voyage au centre de la Terre. Edition Livre de poche, 1864'),
 ('De la Terre à la Lune', 'Jules Verne,De la Terre à la Lune. Edition Livre de poche, 1865'),
 ('Vingt mille lieues sous les mers ', 'Jules Verne, De la Terre à la Lune. Edition Livre de poche, 1870'),
 ('Le Tour du monde en quatre-vingts jours', 'Jules Verne, De la Terre à la Lune. Edition Livre de poche, 1873'),
 ("L'Île mystérieuse", 'Jules Verne, De la Terre à la Lune. Edition Livre de poche, 1874');
 
 INSERT INTO livre (titre, notice)
 VALUES
 ('Cujo', 'Stephen King, Cujo. Edition Albin Michel, 1982'),
 ('Shining', 'Stephen King, Shining. Edition Alta, 1977'),
 ('La ligne verte', 'Stephen King, La ligne verte. Edition Librio, 1996'),
 ('Les Yeux du dragon', 'Stephen King, Les Yeux du dragon. Edition Albin Michel, 1995'),
 ("Salem", 'Stephen King, Salem. Edition Alta, 1977');

 INSERT INTO livre (titre, notice)
 VALUES
 ('La Mystérieuse Affaire de Styles', 'Agatha Christie, La Mystérieuse Affaire de Styles. Edition Livre de poche, 1920'),
 ('Mr Brown', 'Agatha Christie, Mr Brown. Edition Livre de poche, 1922'),
 ("Les Enquêtes d'Hercule Poirot", "Agatha Christie, Les Enquêtes d'Hercule Poirot. Edition Livre de poche, 1924"),
 ('Associés contre le crime', 'Agatha Christie, Associés contre le crime. Edition Pocket, 1929'),
 ("Le couteau sur la Nuque", 'Agatha Christie, Le couteau sur la Nuque. Edition Pocket, 1933');
 
  INSERT INTO livre (titre, notice)
 VALUES
 ("Et si c'était vrai...", "Marc Levy, Et si c'était vrai.. . Edition Robert Laffont, 2000"),
 (' Sept jours pour une éternité...', 'Marc Levy,  Sept jours pour une éternité.... Edition Robert Laffont, 2003'),
 (" Le Premier Jour", "Marc Levy,  Le Premier Jour. Edition Robert Laffont, 2009"),
 (' Une autre idée du bonheur', 'Marc Levy,  Une autre idée du bonheur. Edition Robert Laffont, 2014'),
 (" L'Horizon à l'envers", "Marc Levy,  L'Horizon à l'envers. Edition Robert Laffont, 2016");
 
   INSERT INTO livre (titre, notice)
 VALUES
 ("Bug-Jargal", "Victor Hugo, Bug-Jargal. Edition Livre de poche, 1818"),
 ('Notre-Dame de Paris', 'Victor Hugo, Notre-Dame de Paris. Edition Livre de poche, 1831'),
 ("Le Dernier Jour d'un condamné", "Victor Hugo,  Le Dernier Jour d'un condamné. Edition Livre de poche, 1829"),
 ('Les Misérables', 'Victor Hugo, Les Misérables. Edition Livre de poche, 1862'),
 ("Les Travailleurs de la mer", "Victor Hugo, Les Travailleurs de la mer. Edition Livre de poche, 1866");
 
 --Script d'association livre-auteur
 
    INSERT INTO livre_auteur (livre_id, auteur_id)
 VALUES
(1,1),
(2,1),
(3,1),
(4,1),
(5,1),
(6,2),
(7,2),
(8,2),
(9,2),
(10,2),
(11,3),
(12,3),
(13,3),
(14,3),
(15,3),
(16,4),
(17,4),
(18,4),
(19,4),
(20,4),
(21,5),
(22,5),
(23,5),
(24,5),
(25,5);


--Script d'insertion de theme


INSERT INTO theme (nom, description)
 VALUES
 ('Amour', 'Amour'),
 ('Aventure', 'Aventure'),
 ('Policier', 'Policier'),
 ('Science-Fiction', 'Science-Fiction'),
 ('Manga', 'Manga'),
 ('Horreur', 'Horreur'),
 ('Roman', 'Roman'),
 ('Fiction', 'Fiction');
 
 
 --Script association livre_theme
 INSERT INTO livre_theme (livre_id, theme_id)
 VALUES
(1,4),
(2,4),
(3,4),
(4,2),
(5,4),
(6,6),
(7,6),
(8,4),
(9,6),
(10,6),
(11,3),
(12,3),
(13,3),
(14,3),
(15,3),
(16,7),
(17,7),
(18,7),
(19,7),
(20,7),
(21,8),
(22,7),
(23,7),
(24,7),
(25,7);
 
 
 
 