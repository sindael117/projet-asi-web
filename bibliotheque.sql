-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 29 Mars 2016 à 19:33
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bibliotheque`
--

-- --------------------------------------------------------

--
-- Structure de la table `archive`
--

CREATE TABLE IF NOT EXISTS `archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre_livre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom_prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_debut` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date_fin` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `auteur`
--

CREATE TABLE IF NOT EXISTS `auteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_55AB1406C6E55B5` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

CREATE TABLE IF NOT EXISTS `emprunt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emprunts_exemplaire_id` int(11) DEFAULT NULL,
  `emprunts_inscrit_id` int(11) DEFAULT NULL,
  `date_emprunt` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_364071D78CBC6B15` (`emprunts_exemplaire_id`),
  KEY `IDX_364071D7991974B9` (`emprunts_inscrit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


--
-- Contenu de la table `emprunt`
--
INSERT INTO `emprunt` (`id`,`emprunts_exemplaire_id`,`emprunts_inscrit_id`,`date_emprunt`) VALUES (1,83,38,"11/03/2016"),(2,32,72,"08/04/2016"),(3,32,26,"23/02/2017"),(4,13,36,"01/08/2016"),(5,49,26,"11/05/2015"),(6,21,45,"19/07/2016"),(7,14,60,"18/06/2015"),(8,19,68,"25/04/2016"),(9,55,81,"21/06/2015"),(10,68,43,"07/09/2015");
INSERT INTO `emprunt` (`id`,`emprunts_exemplaire_id`,`emprunts_inscrit_id`,`date_emprunt`) VALUES (11,89,95,"10/06/2016"),(12,62,43,"01/06/2016"),(13,20,92,"10/03/2016"),(14,85,11,"08/10/2016"),(15,44,7,"29/03/2015"),(16,46,54,"20/03/2016"),(17,67,49,"24/05/2016"),(18,100,41,"19/02/2016"),(19,91,40,"13/08/2015"),(20,8,14,"22/08/2016");
INSERT INTO `emprunt` (`id`,`emprunts_exemplaire_id`,`emprunts_inscrit_id`,`date_emprunt`) VALUES (21,24,96,"29/04/2016"),(22,82,14,"06/07/2015"),(23,83,98,"19/07/2016"),(24,9,83,"12/10/2015"),(25,33,5,"31/12/2016"),(26,4,19,"10/08/2016"),(27,60,91,"31/12/2016"),(28,75,12,"06/05/2016"),(29,48,99,"03/01/2017"),(30,14,3,"31/08/2015");
INSERT INTO `emprunt` (`id`,`emprunts_exemplaire_id`,`emprunts_inscrit_id`,`date_emprunt`) VALUES (31,94,48,"12/04/2016"),(32,9,10,"05/10/2015"),(33,8,80,"29/03/2016"),(34,10,39,"11/08/2015"),(35,11,96,"04/11/2016"),(36,7,37,"08/11/2016"),(37,74,3,"09/06/2016"),(38,50,93,"13/12/2015"),(39,97,64,"01/11/2015"),(40,95,47,"04/05/2016");
INSERT INTO `emprunt` (`id`,`emprunts_exemplaire_id`,`emprunts_inscrit_id`,`date_emprunt`) VALUES (41,16,34,"20/06/2015"),(42,52,75,"06/01/2017"),(43,44,52,"18/08/2016"),(44,5,100,"15/09/2016"),(45,72,24,"27/12/2016"),(46,84,23,"02/02/2017"),(47,89,44,"23/08/2016"),(48,61,5,"03/04/2015"),(49,25,71,"03/12/2015"),(50,31,19,"26/09/2016");
INSERT INTO `emprunt` (`id`,`emprunts_exemplaire_id`,`emprunts_inscrit_id`,`date_emprunt`) VALUES (51,51,23,"29/11/2016"),(52,93,8,"19/06/2015"),(53,76,2,"10/11/2015"),(54,65,23,"09/07/2015"),(55,5,61,"21/12/2015"),(56,77,23,"16/10/2016"),(57,20,28,"16/04/2015"),(58,43,99,"07/09/2016"),(59,17,53,"11/05/2015"),(60,25,15,"30/10/2015");
INSERT INTO `emprunt` (`id`,`emprunts_exemplaire_id`,`emprunts_inscrit_id`,`date_emprunt`) VALUES (61,67,42,"20/12/2016"),(62,7,38,"27/08/2015"),(63,63,12,"05/08/2016"),(64,90,86,"12/11/2015"),(65,15,95,"16/11/2016"),(66,17,9,"02/10/2016"),(67,83,82,"01/09/2016"),(68,78,89,"23/07/2015"),(69,64,71,"14/10/2015"),(70,84,2,"20/07/2015");
INSERT INTO `emprunt` (`id`,`emprunts_exemplaire_id`,`emprunts_inscrit_id`,`date_emprunt`) VALUES (71,20,18,"01/09/2016"),(72,60,80,"03/03/2017"),(73,8,58,"15/05/2015"),(74,66,21,"26/03/2017"),(75,20,34,"09/04/2016"),(76,10,10,"05/10/2015"),(77,14,53,"10/08/2016"),(78,55,69,"06/05/2015"),(79,73,10,"24/04/2015"),(80,86,29,"02/08/2015");
INSERT INTO `emprunt` (`id`,`emprunts_exemplaire_id`,`emprunts_inscrit_id`,`date_emprunt`) VALUES (81,72,38,"31/01/2017"),(82,4,84,"02/04/2015"),(83,19,84,"10/02/2017"),(84,65,7,"09/03/2016"),(85,43,60,"24/05/2016"),(86,89,53,"23/04/2016"),(87,79,91,"10/01/2016"),(88,10,20,"15/10/2015"),(89,53,18,"27/05/2016"),(90,77,25,"09/06/2016");
INSERT INTO `emprunt` (`id`,`emprunts_exemplaire_id`,`emprunts_inscrit_id`,`date_emprunt`) VALUES (91,72,89,"04/11/2015"),(92,28,63,"15/01/2016"),(93,95,40,"20/09/2015"),(94,53,14,"22/08/2015"),(95,70,44,"07/07/2015"),(96,24,25,"04/12/2015"),(97,27,59,"06/09/2016"),(98,11,14,"24/04/2015"),(99,96,78,"29/08/2015"),(100,13,1,"09/09/2015");




-- --------------------------------------------------------

--
-- Structure de la table `etagere`
--

CREATE TABLE IF NOT EXISTS `etagere` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `etageres_rayon_id` int(11) DEFAULT NULL,
  `numero` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B83FE5C46C6E55B5` (`nom`),
  KEY `IDX_B83FE5C4AB9F0318` (`etageres_rayon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


INSERT INTO `etagere` (`id`,`etageres_rayon_id`,`numero`) VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6);
INSERT INTO `etagere` (`id`,`etageres_rayon_id`,`numero`) VALUES (7,2,1),(8,2,2),(9,2,3),(10,2,4),(11,2,5),(12,2,6);
INSERT INTO `etagere` (`id`,`etageres_rayon_id`,`numero`) VALUES (13,3,1),(14,3,2),(15,3,3),(16,3,4),(17,3,5),(18,3,6);
INSERT INTO `etagere` (`id`,`etageres_rayon_id`,`numero`) VALUES (19,4,1),(20,4,2),(21,4,3),(22,4,4),(23,4,5),(24,4,6);
INSERT INTO `etagere` (`id`,`etageres_rayon_id`,`numero`) VALUES (25,5,1),(26,5,2),(27,5,3),(28,5,4),(29,5,5),(30,5,6);

-- --------------------------------------------------------

--
-- Structure de la table `exemplaire`
--

CREATE TABLE IF NOT EXISTS `exemplaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exemplaires_etagere_id` int(11) DEFAULT NULL,
  `exemplaires_livre_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5EF83C925EDA96AE` (`exemplaires_etagere_id`),
  KEY `IDX_5EF83C9253DBD4EF` (`exemplaires_livre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;



-- --------------------------------------------------------

--
-- Structure de la table `inscrit`
--

CREATE TABLE IF NOT EXISTS `inscrit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `faculte` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `cycle` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=101 ;

--
-- Contenu de la table `inscrit`
--

INSERT INTO `inscrit` (`id`, `nom`, `prenom`, `faculte`, `cycle`) VALUES
(1, 'Rama', 'Timothy', 'Informatique', '4'),
(2, 'Teagan', 'Arden', 'Economie', '2'),
(3, 'Jelani', 'Mariam', 'Médecine', '3'),
(4, 'Lois', 'Noble', 'Economie', '1'),
(5, 'Denise', 'Leandra', 'Histoire', '1'),
(6, 'Ella', 'Wanda', 'Lettres', '5'),
(7, 'Ciara', 'Natalie', 'Droit', '4'),
(8, 'Hollee', 'Carla', 'Droit', '2'),
(9, 'Hedwig', 'Uma', 'Lettres', '1'),
(10, 'Charde', 'Joseph', 'Economie', '3'),
(11, 'Anthony', 'Charles', 'Histoire', '5'),
(12, 'Maya', 'Ramona', 'Histoire', '1'),
(13, 'Chanda', 'Joshua', 'Médecine', '5'),
(14, 'Holmes', 'Karyn', 'Médecine', '4'),
(15, 'Indira', 'Brendan', 'Droit', '3'),
(16, 'Yolanda', 'Rogan', 'Médecine', '3'),
(17, 'Selma', 'Alec', 'Lettres', '4'),
(18, 'Mechelle', 'Aphrodite', 'Economie', '2'),
(19, 'Vivien', 'Chester', 'Histoire', '5'),
(20, 'Rose', 'Kevyn', 'Informatique', '4'),
(21, 'Colorado', 'Lewis', 'STAPS', '3'),
(22, 'Carter', 'Yuli', 'Informatique', '5'),
(23, 'Audrey', 'Kaitlin', 'Médecine', '1'),
(24, 'Madonna', 'Shana', 'Lettres', '4'),
(25, 'Grady', 'Dustin', 'Médecine', '2'),
(26, 'Rina', 'Nathan', 'Droit', '4'),
(27, 'Anthony', 'Alice', 'Lettres', '3'),
(28, 'Aline', 'Morgan', 'STAPS', '1'),
(29, 'Aquila', 'Nell', 'Droit', '1'),
(30, 'Mikayla', 'Zahir', 'Informatique', '2'),
(31, 'Amos', 'Noelle', 'Droit', '5'),
(32, 'Hilel', 'Quintessa', 'Economie', '1'),
(33, 'Darius', 'Morgan', 'Economie', '4'),
(34, 'Rahim', 'Kathleen', 'Lettres', '5'),
(35, 'Roanna', 'Dorian', 'Lettres', '3'),
(36, 'Keiko', 'Belle', 'Droit', '4'),
(37, 'Cally', 'Sara', 'STAPS', '2'),
(38, 'Tad', 'Jeanette', 'Médecine', '5'),
(39, 'Jeanette', 'Yuli', 'Histoire', '2'),
(40, 'Julian', 'Neil', 'Informatique', '3'),
(41, 'Liberty', 'Nerea', 'Lettres', '4'),
(42, 'Austin', 'Holly', 'Médecine', '2'),
(43, 'Daria', 'Dante', 'Médecine', '1'),
(44, 'Vivian', 'Blaine', 'Lettres', '5'),
(45, 'Flavia', 'Wyoming', 'Histoire', '1'),
(46, 'Fletcher', 'Shelby', 'Droit', '3'),
(47, 'Venus', 'Hadley', 'Médecine', '3'),
(48, 'Alfonso', 'Ima', 'Informatique', '1'),
(49, 'Sonya', 'Salvador', 'Histoire', '4'),
(50, 'Hasad', 'Alisa', 'Economie', '3'),
(51, 'Amery', 'Raja', 'STAPS', '3'),
(52, 'Cecilia', 'Arthur', 'Economie', '4'),
(53, 'Dante', 'Delilah', 'Droit', '2'),
(54, 'Samson', 'Amelia', 'Médecine', '4'),
(55, 'Inga', 'Alyssa', 'STAPS', '5'),
(56, 'Herman', 'Casey', 'Histoire', '3'),
(57, 'Francis', 'Shad', 'STAPS', '5'),
(58, 'Axel', 'Xerxes', 'Informatique', '2'),
(59, 'Ian', 'Basil', 'Economie', '3'),
(60, 'Remedios', 'Tanner', 'Lettres', '5'),
(61, 'Amir', 'Sydnee', 'Histoire', '3'),
(62, 'Keefe', 'Fredericka', 'Droit', '3'),
(63, 'Brent', 'Timon', 'STAPS', '3'),
(64, 'Lydia', 'Thor', 'Droit', '1'),
(65, 'Derek', 'Lillith', 'Lettres', '4'),
(66, 'Ramona', 'Miranda', 'Lettres', '4'),
(67, 'Aiko', 'Rhona', 'Droit', '2'),
(68, 'Eleanor', 'Debra', 'Droit', '5'),
(69, 'Charlotte', 'Aurora', 'Droit', '1'),
(70, 'MacKensie', 'Illana', 'STAPS', '1'),
(71, 'Fulton', 'Beverly', 'STAPS', '2'),
(72, 'Yoshi', 'Axel', 'Droit', '2'),
(73, 'Mannix', 'Noah', 'Histoire', '3'),
(74, 'Whilemina', 'Quinlan', 'STAPS', '1'),
(75, 'Reagan', 'Aubrey', 'Histoire', '1'),
(76, 'Briar', 'Bruno', 'STAPS', '5'),
(77, 'Madonna', 'Hamilton', 'Histoire', '5'),
(78, 'Mallory', 'Stella', 'Informatique', '1'),
(79, 'Edward', 'Autumn', 'Lettres', '2'),
(80, 'Hamilton', 'Heidi', 'Economie', '3'),
(81, 'Tatiana', 'Barrett', 'Lettres', '3'),
(82, 'Grace', 'Fitzgerald', 'STAPS', '1'),
(83, 'Perry', 'Brent', 'Médecine', '5'),
(84, 'Caesar', 'Myra', 'Informatique', '1'),
(85, 'Nell', 'Callie', 'Economie', '4'),
(86, 'Ira', 'Jolene', 'Informatique', '5'),
(87, 'Carly', 'Amethyst', 'Lettres', '5'),
(88, 'Julian', 'Quail', 'Droit', '2'),
(89, 'Thomas', 'Brendan', 'STAPS', '3'),
(90, 'Sebastian', 'Azalia', 'Médecine', '1'),
(91, 'Aiko', 'Grace', 'Informatique', '5'),
(92, 'Quin', 'Vernon', 'Médecine', '5'),
(93, 'Kareem', 'Dane', 'Médecine', '5'),
(94, 'Dolan', 'Haley', 'Informatique', '4'),
(95, 'Olga', 'Shellie', 'Droit', '5'),
(96, 'Kirestin', 'Ayanna', 'Lettres', '4'),
(97, 'Felicia', 'Alea', 'Histoire', '1'),
(98, 'Margaret', 'Venus', 'Economie', '3'),
(99, 'Joel', 'Lester', 'STAPS', '1'),
(100, 'Ignacia', 'Delilah', 'STAPS', '2');

-- --------------------------------------------------------

--
-- Structure de la table `livre`
--

CREATE TABLE IF NOT EXISTS `livre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cote` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=101 ;

--
-- Contenu de la table `livre`
--

INSERT INTO `livre` (`id`, `titre`, `notice`, `cote`) VALUES
(1, 'dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit', 'dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere, enim nisl elementum purus, accumsan interdum libero', 'O079279'),
(2, 'Praesent eu nulla at sem molestie sodales. Mauris blandit', 'pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit', 'I971558'),
(3, 'Aliquam auctor, velit eget laoreet posuere, enim nisl', 'mauris, aliquam eu, accumsan sed, facilisis', 'M593852'),
(4, 'vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor', 'Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere,', 'L035803'),
(5, 'hendrerit neque. In', 'imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo', 'P872279'),
(6, 'amet massa.', 'Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis', 'G580115'),
(7, 'amet,', 'Morbi neque tellus, imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor', 'N255476'),
(8, 'metus.', 'pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac', 'U734785'),
(9, 'Nullam suscipit, est ac facilisis facilisis, magna tellus', 'libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id', 'S702641'),
(10, 'tempor lorem, eget mollis lectus pede et risus. Quisque', 'gravida sit amet, dapibus id, blandit', 'I528003'),
(11, 'imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam', 'molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat', 'G858054'),
(12, 'ac metus vitae', 'amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem', 'Q260923'),
(13, 'nec, diam. Duis', 'metus. Aenean sed', 'O902541'),
(14, 'lectus ante dictum mi, ac mattis', 'Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non,', 'R226406'),
(15, 'consectetuer, cursus', 'Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum', 'I365255'),
(16, 'dui,', 'Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis', 'F798274'),
(17, 'varius orci, in consequat enim diam', 'libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus', 'A774927'),
(18, 'lorem,', 'odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae', 'L883862'),
(19, 'laoreet', 'sagittis felis. Donec tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi', 'F413875'),
(20, 'Duis a mi fringilla mi lacinia', 'Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque', 'C449393'),
(21, 'vitae odio sagittis semper. Nam', 'orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis', 'G829374'),
(22, 'ut lacus. Nulla', 'mi', 'I902319'),
(23, 'elementum', 'Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum.', 'S932186'),
(24, 'sed, sapien. Nunc pulvinar arcu et', 'a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at', 'X482745'),
(25, 'dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales', 'Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat', 'O459018'),
(26, 'enim nisl elementum purus, accumsan interdum libero dui nec', 'ultricies adipiscing, enim mi tempor lorem, eget mollis lectus', 'X227648'),
(27, 'neque venenatis lacus.', 'aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing', 'T328645'),
(28, 'quam. Pellentesque habitant morbi tristique senectus et', 'mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu enim. Etiam', 'E145590'),
(29, 'lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum', 'nisl sem,', 'E383041'),
(30, 'risus odio, auctor vitae, aliquet nec, imperdiet nec, leo.', 'dignissim. Maecenas ornare egestas ligula.', 'C680827'),
(31, 'Quisque purus', 'ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas malesuada fringilla est. Mauris eu turpis. Nulla', 'I436199'),
(32, 'pede sagittis augue, eu', 'tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus', 'L513686'),
(33, 'elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut', 'eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis,', 'I454297'),
(34, 'purus.', 'venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec', 'E651978'),
(35, 'metus. Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper', 'Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec', 'S772619'),
(36, 'sagittis. Nullam vitae diam. Proin', 'Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna', 'A611686'),
(37, 'neque. Sed eget lacus. Mauris', 'pretium neque. Morbi quis urna. Nunc quis arcu vel quam dignissim pharetra. Nam ac nulla. In tincidunt congue turpis. In condimentum.', 'H238285'),
(38, 'commodo tincidunt', 'commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in,', 'E790688'),
(39, 'Duis elementum, dui quis accumsan convallis, ante lectus', 'cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit,', 'S247290'),
(40, 'Maecenas', 'Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis', 'N903881'),
(41, 'congue, elit sed consequat auctor, nunc nulla vulputate', 'adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor', 'Z222504'),
(42, 'nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus', 'Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula.', 'O903334'),
(43, 'Sed id risus quis diam luctus lobortis. Class', 'ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis.', 'S907035'),
(44, 'dolor sit amet, consectetuer', 'convallis, ante lectus convallis est, vitae sodales', 'U860496'),
(45, 'faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus.', 'condimentum. Donec at arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel pede', 'P354363'),
(46, 'ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing', 'non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim.', 'O094672'),
(47, 'lorem ipsum sodales purus, in molestie tortor', 'Donec sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin', 'W097385'),
(48, 'tortor. Integer aliquam adipiscing lacus.', 'per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet ornare. In', 'K908027'),
(49, 'interdum enim non nisi. Aenean eget metus. In', 'Sed molestie. Sed id risus quis diam luctus lobortis. Class', 'Z399726'),
(50, 'Curae; Donec', 'gravida molestie arcu. Sed eu', 'I062440'),
(51, 'ac orci. Ut', 'justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis nec mauris blandit mattis. Cras', 'T627181'),
(52, 'luctus', 'enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus.', 'B495125'),
(53, 'pede, ultrices a, auctor non, feugiat', 'fermentum convallis ligula. Donec luctus aliquet odio. Etiam', 'C985285'),
(54, 'Etiam ligula tortor, dictum', 'odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante.', 'O449633'),
(55, 'neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin', 'consectetuer rhoncus. Nullam velit dui, semper et, lacinia', 'A912643'),
(56, 'luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae', 'semper tellus id', 'Q809600'),
(57, 'nec, euismod in, dolor.', 'ipsum leo', 'D563055'),
(58, 'eu', 'velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin', 'Z476139'),
(59, 'a, arcu. Sed et libero. Proin mi. Aliquam', 'cursus. Nunc mauris elit,', 'A659579'),
(60, 'metus eu', 'dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus', 'H313665'),
(61, 'tempor lorem,', 'ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque', 'C901931'),
(62, 'semper', 'est ac mattis semper, dui lectus rutrum urna, nec luctus felis', 'J833000'),
(63, 'dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris,', 'interdum.', 'Z998197'),
(64, 'tempor augue ac ipsum. Phasellus vitae mauris', 'enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor', 'O005707'),
(65, 'tristique neque venenatis', 'In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet', 'L514110'),
(66, 'urna justo faucibus lectus, a sollicitudin orci sem eget', 'ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed', 'X227563'),
(67, 'velit.', 'dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas', 'I761186'),
(68, 'vel', 'mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem', 'X786753'),
(69, 'facilisis, magna', 'lectus justo eu arcu. Morbi sit amet massa.', 'V032506'),
(70, 'varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas', 'a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum.', 'M553751'),
(71, 'nisi', 'iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus', 'E676059'),
(72, 'nulla ante, iaculis nec, eleifend non,', 'lorem, sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis dis parturient', 'H156071'),
(73, 'ac nulla. In', 'Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui.', 'F086005'),
(74, 'sem magna nec quam. Curabitur vel lectus. Cum', 'iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus.', 'N607519'),
(75, 'erat semper rutrum. Fusce dolor quam, elementum at,', 'egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec', 'D578056'),
(76, 'aliquet magna a neque.', 'erat semper rutrum. Fusce dolor quam, elementum at, egestas a,', 'R009765'),
(77, 'elit erat vitae', 'auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id', 'R410083'),
(78, 'nec ante. Maecenas mi', 'neque vitae semper egestas, urna', 'L967820'),
(79, 'metus eu erat semper rutrum. Fusce dolor quam, elementum at,', 'orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque', 'G008183'),
(80, 'egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla', 'non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu', 'F003980'),
(81, 'adipiscing elit. Aliquam auctor, velit', 'eget metus. In nec orci. Donec', 'S106064'),
(82, 'metus sit amet ante. Vivamus non lorem', 'tellus id nunc', 'M867572'),
(83, 'Sed eu eros. Nam consequat', 'ac arcu. Nunc', 'E251315'),
(84, 'aliquet. Proin velit. Sed malesuada augue ut', 'Nunc sollicitudin', 'V457505'),
(85, 'a tortor. Nunc commodo', 'vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id', 'Q809710'),
(86, 'turpis. Nulla aliquet.', 'mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare', 'D296117'),
(87, 'consectetuer mauris id sapien. Cras dolor dolor, tempus', 'amet luctus vulputate, nisi sem semper', 'C210015'),
(88, 'porttitor tellus non magna. Nam ligula elit, pretium et, rutrum', 'Aenean gravida nunc sed pede. Cum sociis', 'W269674'),
(89, 'Vivamus molestie dapibus ligula. Aliquam erat', 'et,', 'A397672'),
(90, 'elit, a feugiat tellus lorem eu metus. In lorem. Donec', 'nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient', 'N141516'),
(91, 'elementum, lorem ut aliquam iaculis,', 'pellentesque a, facilisis', 'U479630'),
(92, 'Cras eu tellus eu augue porttitor interdum.', 'magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur', 'P737186'),
(93, 'Mauris quis', 'lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus', 'Q205568'),
(94, 'In scelerisque scelerisque dui. Suspendisse ac metus vitae velit', 'ac nulla. In tincidunt congue turpis. In', 'A723008'),
(95, 'turpis non enim. Mauris quis turpis vitae purus gravida sagittis.', 'ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas', 'Q383732'),
(96, 'ridiculus', 'lacus vestibulum lorem, sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et', 'P387709'),
(97, 'eget nisi dictum augue malesuada', 'cursus in, hendrerit', 'O092441'),
(98, 'egestas ligula. Nullam feugiat placerat velit.', 'Praesent interdum ligula eu enim. Etiam imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus', 'K915390'),
(99, 'mi lacinia mattis. Integer eu lacus.', 'vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis', 'I700562'),
(100, 'sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla', 'vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan', 'R153593');

-- --------------------------------------------------------

--
-- Structure de la table `livre_auteur`
--

CREATE TABLE IF NOT EXISTS `livre_auteur` (
  `livre_id` int(11) NOT NULL,
  `auteur_id` int(11) NOT NULL,
  PRIMARY KEY (`livre_id`,`auteur_id`),
  KEY `IDX_A11876B537D925CB` (`livre_id`),
  KEY `IDX_A11876B560BB6FE6` (`auteur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `livre_theme`
--

CREATE TABLE IF NOT EXISTS `livre_theme` (
  `livre_id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  PRIMARY KEY (`livre_id`,`theme_id`),
  KEY `IDX_4767F6E37D925CB` (`livre_id`),
  KEY `IDX_4767F6E59027487` (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `livre_theme`
--

INSERT INTO `livre_theme` (`livre_id`, `theme_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(9, 1),
(9, 2),
(10, 1),
(10, 2),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(17, 1),
(17, 2),
(18, 1),
(18, 2),
(19, 1),
(19, 2),
(20, 1),
(20, 2),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 3),
(26, 3),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(33, 3),
(34, 3),
(35, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(40, 3);

-- --------------------------------------------------------

--
-- Structure de la table `rayon`
--

CREATE TABLE IF NOT EXISTS `rayon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rayons_theme_id` int(11) DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D5E5BC3C6C6E55B5` (`nom`),
  KEY `IDX_D5E5BC3C816B257C` (`rayons_theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


INSERT INTO `rayon` (`id`, `rayons_theme_id`, `designation`) VALUES
(1, '1', 'SF'),
(2, '2', 'POLICIER'),
(3, '3', 'FANTAST'),
(4, '5', 'CLASSIQUE'),
(5, '6', 'APPRENTISSAGE');
-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservations_exemplaire_id` int(11) DEFAULT NULL,
  `reservations_inscrit_id` int(11) DEFAULT NULL,
  `date_reservation` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_42C849552F173C4C` (`reservations_exemplaire_id`),
  KEY `IDX_42C84955DFD1F4B3` (`reservations_inscrit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


--
-- Contenu de la table `reservation`
--
INSERT INTO `reservation` (`id`,`reservations_exemplaire_id`,`reservations_inscrit_id`,`date_reservation`) VALUES (1,49,91,"15/03/2017"),(2,71,92,"04/09/2015"),(3,90,15,"25/04/2016"),(4,66,82,"15/12/2016"),(5,12,33,"12/01/2017"),(6,9,21,"06/02/2016"),(7,87,25,"24/12/2016"),(8,44,53,"29/04/2015"),(9,75,31,"08/03/2017"),(10,87,6,"12/01/2016");
INSERT INTO `reservation` (`id`,`reservations_exemplaire_id`,`reservations_inscrit_id`,`date_reservation`) VALUES (11,11,36,"13/04/2015"),(12,94,87,"01/07/2015"),(13,46,28,"25/09/2015"),(14,100,39,"29/01/2016"),(15,58,13,"03/05/2016"),(16,47,49,"22/04/2015"),(17,8,34,"13/02/2017"),(18,59,37,"13/03/2017"),(19,67,15,"13/04/2016"),(20,10,3,"11/06/2016");
INSERT INTO `reservation` (`id`,`reservations_exemplaire_id`,`reservations_inscrit_id`,`date_reservation`) VALUES (21,17,17,"13/12/2016"),(22,54,20,"17/07/2015"),(23,42,14,"20/02/2016"),(24,88,29,"23/11/2015"),(25,89,63,"03/07/2015"),(26,63,23,"01/12/2015"),(27,11,73,"29/06/2016"),(28,57,70,"01/12/2016"),(29,76,89,"07/05/2015"),(30,19,91,"24/08/2016");
INSERT INTO `reservation` (`id`,`reservations_exemplaire_id`,`reservations_inscrit_id`,`date_reservation`) VALUES (31,16,66,"26/02/2016"),(32,40,3,"14/05/2015"),(33,49,90,"25/02/2016"),(34,100,33,"26/01/2016"),(35,48,40,"20/10/2015"),(36,66,72,"23/08/2015"),(37,54,75,"26/07/2015"),(38,60,96,"18/11/2015"),(39,24,34,"16/04/2015"),(40,76,34,"14/12/2015");
INSERT INTO `reservation` (`id`,`reservations_exemplaire_id`,`reservations_inscrit_id`,`date_reservation`) VALUES (41,14,99,"31/10/2016"),(42,41,79,"09/04/2016"),(43,79,95,"17/07/2015"),(44,96,16,"01/05/2016"),(45,62,22,"22/12/2016"),(46,48,8,"20/03/2017"),(47,17,85,"28/03/2017"),(48,20,34,"26/01/2017"),(49,83,57,"29/05/2016"),(50,13,68,"06/10/2015");
INSERT INTO `reservation` (`id`,`reservations_exemplaire_id`,`reservations_inscrit_id`,`date_reservation`) VALUES (51,69,93,"10/04/2015"),(52,99,74,"26/11/2016"),(53,16,29,"27/02/2016"),(54,62,81,"16/08/2016"),(55,10,21,"14/06/2016"),(56,2,40,"15/09/2015"),(57,90,59,"07/11/2015"),(58,93,86,"12/03/2017"),(59,52,75,"12/10/2015"),(60,38,1,"06/12/2016");
INSERT INTO `reservation` (`id`,`reservations_exemplaire_id`,`reservations_inscrit_id`,`date_reservation`) VALUES (61,21,17,"01/08/2016"),(62,91,85,"18/12/2016"),(63,83,75,"14/07/2016"),(64,85,61,"06/09/2015"),(65,71,45,"23/06/2015"),(66,18,91,"18/06/2015"),(67,68,27,"03/10/2015"),(68,14,93,"16/12/2015"),(69,84,58,"13/12/2016"),(70,25,88,"31/07/2015");
INSERT INTO `reservation` (`id`,`reservations_exemplaire_id`,`reservations_inscrit_id`,`date_reservation`) VALUES (71,37,67,"09/09/2015"),(72,82,49,"25/01/2016"),(73,51,63,"29/10/2015"),(74,47,71,"24/08/2015"),(75,58,27,"01/06/2016"),(76,43,11,"07/04/2016"),(77,83,86,"10/03/2016"),(78,7,89,"01/12/2015"),(79,76,92,"30/12/2015"),(80,40,13,"22/01/2016");
INSERT INTO `reservation` (`id`,`reservations_exemplaire_id`,`reservations_inscrit_id`,`date_reservation`) VALUES (81,71,90,"14/02/2016"),(82,64,3,"27/12/2016"),(83,52,16,"04/03/2017"),(84,47,90,"15/11/2015"),(85,52,46,"04/12/2015"),(86,77,95,"12/10/2015"),(87,22,15,"28/02/2016"),(88,4,76,"10/12/2016"),(89,14,80,"27/02/2016"),(90,82,100,"24/06/2016");
INSERT INTO `reservation` (`id`,`reservations_exemplaire_id`,`reservations_inscrit_id`,`date_reservation`) VALUES (91,26,73,"19/01/2016"),(92,26,83,"12/09/2016"),(93,7,9,"10/01/2016"),(94,45,13,"15/08/2016"),(95,52,6,"28/02/2016"),(96,2,41,"24/04/2016"),(97,36,17,"28/04/2016"),(98,99,74,"10/12/2015"),(99,18,94,"31/03/2016"),(100,72,70,"12/02/2016");




-- --------------------------------------------------------

--
-- Structure de la table `theme`
--

CREATE TABLE IF NOT EXISTS `theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9775E7086C6E55B5` (`nom`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Contenu de la table `theme`
--

INSERT INTO `theme` (`id`, `nom`, `description`) VALUES
(1, 'Science-fiction', 'La science-fiction est structurée par des hypothèses sur ce que pourrait être le futur ou ce qu''aurait pu être le présent voire le passé.'),
(2, 'Policier', 'Le roman policier est fondé sur l''attention d''un fait ou, plus précisément, d''une intrigue, et sur une recherche méthodique faite de preuves, le plus souvent par une enquête policière ou encore une enquête de détective privé'),
(3, 'Fantastique', 'Le fantastique est un registre littéraire qui se caractérise par l’intrusion du surnaturel dans le cadre réaliste pour un récit et qui fait souvent intervenir la magie.  '),
(5, 'Classique', 'Thème regroupant tous les grands livres de la littérature.'),
(6, 'Apprentissage', 'Livre contenant des cours et des exercices pour parfaire ses connaissances.');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `emprunt`
--
ALTER TABLE `emprunt`
  ADD CONSTRAINT `FK_364071D78CBC6B15` FOREIGN KEY (`emprunts_exemplaire_id`) REFERENCES `exemplaire` (`id`),
  ADD CONSTRAINT `FK_364071D7991974B9` FOREIGN KEY (`emprunts_inscrit_id`) REFERENCES `inscrit` (`id`);

--
-- Contraintes pour la table `etagere`
--
ALTER TABLE `etagere`
  ADD CONSTRAINT `FK_B83FE5C4AB9F0318` FOREIGN KEY (`etageres_rayon_id`) REFERENCES `rayon` (`id`);

--
-- Contraintes pour la table `exemplaire`
--
ALTER TABLE `exemplaire`
  ADD CONSTRAINT `FK_5EF83C9253DBD4EF` FOREIGN KEY (`exemplaires_livre_id`) REFERENCES `livre` (`id`),
  ADD CONSTRAINT `FK_5EF83C925EDA96AE` FOREIGN KEY (`exemplaires_etagere_id`) REFERENCES `etagere` (`id`);

--
-- Contraintes pour la table `livre_auteur`
--
ALTER TABLE `livre_auteur`
  ADD CONSTRAINT `FK_A11876B537D925CB` FOREIGN KEY (`livre_id`) REFERENCES `livre` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_A11876B560BB6FE6` FOREIGN KEY (`auteur_id`) REFERENCES `auteur` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `livre_theme`
--
ALTER TABLE `livre_theme`
  ADD CONSTRAINT `FK_4767F6E37D925CB` FOREIGN KEY (`livre_id`) REFERENCES `livre` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_4767F6E59027487` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `rayon`
--
ALTER TABLE `rayon`
  ADD CONSTRAINT `FK_D5E5BC3C816B257C` FOREIGN KEY (`rayons_theme_id`) REFERENCES `theme` (`id`);

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK_42C849552F173C4C` FOREIGN KEY (`reservations_exemplaire_id`) REFERENCES `exemplaire` (`id`),
  ADD CONSTRAINT `FK_42C84955DFD1F4B3` FOREIGN KEY (`reservations_inscrit_id`) REFERENCES `inscrit` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
