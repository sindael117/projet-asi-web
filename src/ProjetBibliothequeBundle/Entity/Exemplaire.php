<?php

namespace ProjetBibliothequeBundle\Entity;

/**
 * Exemplaire
 */
class Exemplaire
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dispo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $exemplaireEmprunts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $exemplaireReservations;

    /**
     * @var \ProjetBibliothequeBundle\Entity\Etagere
     */
    private $exemplairesEtagere;

    /**
     * @var \ProjetBibliothequeBundle\Entity\Livre
     */
    private $exemplairesLivre;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->exemplaireEmprunts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->exemplaireReservations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dispo
     *
     * @param integer $dispo
     *
     * @return Exemplaire
     */
    public function setDispo($dispo)
    {
        $this->dispo = $dispo;

        return $this;
    }

    /**
     * Get dispo
     *
     * @return integer
     */
    public function getDispo()
    {
        return $this->dispo;
    }

    /**
     * Add exemplaireEmprunt
     *
     * @param \ProjetBibliothequeBundle\Entity\Emprunt $exemplaireEmprunt
     *
     * @return Exemplaire
     */
    public function addExemplaireEmprunt(\ProjetBibliothequeBundle\Entity\Emprunt $exemplaireEmprunt)
    {
        $this->exemplaireEmprunts[] = $exemplaireEmprunt;

        return $this;
    }

    /**
     * Remove exemplaireEmprunt
     *
     * @param \ProjetBibliothequeBundle\Entity\Emprunt $exemplaireEmprunt
     */
    public function removeExemplaireEmprunt(\ProjetBibliothequeBundle\Entity\Emprunt $exemplaireEmprunt)
    {
        $this->exemplaireEmprunts->removeElement($exemplaireEmprunt);
    }

    /**
     * Get exemplaireEmprunts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExemplaireEmprunts()
    {
        return $this->exemplaireEmprunts;
    }

    /**
     * Add exemplaireReservation
     *
     * @param \ProjetBibliothequeBundle\Entity\Reservation $exemplaireReservation
     *
     * @return Exemplaire
     */
    public function addExemplaireReservation(\ProjetBibliothequeBundle\Entity\Reservation $exemplaireReservation)
    {
        $this->exemplaireReservations[] = $exemplaireReservation;

        return $this;
    }

    /**
     * Remove exemplaireReservation
     *
     * @param \ProjetBibliothequeBundle\Entity\Reservation $exemplaireReservation
     */
    public function removeExemplaireReservation(\ProjetBibliothequeBundle\Entity\Reservation $exemplaireReservation)
    {
        $this->exemplaireReservations->removeElement($exemplaireReservation);
    }

    /**
     * Get exemplaireReservations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExemplaireReservations()
    {
        return $this->exemplaireReservations;
    }

    /**
     * Set exemplairesEtagere
     *
     * @param \ProjetBibliothequeBundle\Entity\Etagere $exemplairesEtagere
     *
     * @return Exemplaire
     */
    public function setExemplairesEtagere(\ProjetBibliothequeBundle\Entity\Etagere $exemplairesEtagere = null)
    {
        $this->exemplairesEtagere = $exemplairesEtagere;

        return $this;
    }

    /**
     * Get exemplairesEtagere
     *
     * @return \ProjetBibliothequeBundle\Entity\Etagere
     */
    public function getExemplairesEtagere()
    {
        return $this->exemplairesEtagere;
    }

    /**
     * Set exemplairesLivre
     *
     * @param \ProjetBibliothequeBundle\Entity\Livre $exemplairesLivre
     *
     * @return Exemplaire
     */
    public function setExemplairesLivre(\ProjetBibliothequeBundle\Entity\Livre $exemplairesLivre = null)
    {
        $this->exemplairesLivre = $exemplairesLivre;

        return $this;
    }

    /**
     * Get exemplairesLivre
     *
     * @return \ProjetBibliothequeBundle\Entity\Livre
     */
    public function getExemplairesLivre()
    {
        return $this->exemplairesLivre;
    }
}
