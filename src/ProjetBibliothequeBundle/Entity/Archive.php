<?php

namespace ProjetBibliothequeBundle\Entity;

/**
 * Archive
 */
class Archive
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $titreLivre;

    /**
     * @var string
     */
    private $nomPrenom;

    /**
     * @var string
     */
    private $dateDebut;

    /**
     * @var string
     */
    private $dateFin;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titreLivre
     *
     * @param string $titreLivre
     *
     * @return Archive
     */
    public function setTitreLivre($titreLivre)
    {
        $this->titreLivre = $titreLivre;

        return $this;
    }

    /**
     * Get titreLivre
     *
     * @return string
     */
    public function getTitreLivre()
    {
        return $this->titreLivre;
    }

    /**
     * Set nomPrenom
     *
     * @param string $nomPrenom
     *
     * @return Archive
     */
    public function setNomPrenom($nomPrenom)
    {
        $this->nomPrenom = $nomPrenom;

        return $this;
    }

    /**
     * Get nomPrenom
     *
     * @return string
     */
    public function getNomPrenom()
    {
        return $this->nomPrenom;
    }

    /**
     * Set dateDebut
     *
     * @param string $dateDebut
     *
     * @return Archive
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return string
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param string $dateFin
     *
     * @return Archive
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return string
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }
}
