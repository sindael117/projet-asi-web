<?php

namespace ProjetBibliothequeBundle\Entity;

/**
 * Inscrit
 */
class Inscrit
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var string
     */
    private $cycle;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $inscritEmprunts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $inscritReservations;

    /**
     * @var \ProjetBibliothequeBundle\Entity\Faculte
     */
    private $inscritFaculte;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->inscritEmprunts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->inscritReservations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Inscrit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Inscrit
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set cycle
     *
     * @param string $cycle
     *
     * @return Inscrit
     */
    public function setCycle($cycle)
    {
        $this->cycle = $cycle;

        return $this;
    }

    /**
     * Get cycle
     *
     * @return string
     */
    public function getCycle()
    {
        return $this->cycle;
    }

    /**
     * Add inscritEmprunt
     *
     * @param \ProjetBibliothequeBundle\Entity\Emprunt $inscritEmprunt
     *
     * @return Inscrit
     */
    public function addInscritEmprunt(\ProjetBibliothequeBundle\Entity\Emprunt $inscritEmprunt)
    {
        $this->inscritEmprunts[] = $inscritEmprunt;

        return $this;
    }

    /**
     * Remove inscritEmprunt
     *
     * @param \ProjetBibliothequeBundle\Entity\Emprunt $inscritEmprunt
     */
    public function removeInscritEmprunt(\ProjetBibliothequeBundle\Entity\Emprunt $inscritEmprunt)
    {
        $this->inscritEmprunts->removeElement($inscritEmprunt);
    }

    /**
     * Get inscritEmprunts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscritEmprunts()
    {
        return $this->inscritEmprunts;
    }

    /**
     * Add inscritReservation
     *
     * @param \ProjetBibliothequeBundle\Entity\Reservation $inscritReservation
     *
     * @return Inscrit
     */
    public function addInscritReservation(\ProjetBibliothequeBundle\Entity\Reservation $inscritReservation)
    {
        $this->inscritReservations[] = $inscritReservation;

        return $this;
    }

    /**
     * Remove inscritReservation
     *
     * @param \ProjetBibliothequeBundle\Entity\Reservation $inscritReservation
     */
    public function removeInscritReservation(\ProjetBibliothequeBundle\Entity\Reservation $inscritReservation)
    {
        $this->inscritReservations->removeElement($inscritReservation);
    }

    /**
     * Get inscritReservations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscritReservations()
    {
        return $this->inscritReservations;
    }

    /**
     * Set inscritFaculte
     *
     * @param \ProjetBibliothequeBundle\Entity\Faculte $inscritFaculte
     *
     * @return Inscrit
     */
    public function setInscritFaculte(\ProjetBibliothequeBundle\Entity\Faculte $inscritFaculte = null)
    {
        $this->inscritFaculte = $inscritFaculte;

        return $this;
    }

    /**
     * Get inscritFaculte
     *
     * @return \ProjetBibliothequeBundle\Entity\Faculte
     */
    public function getInscritFaculte()
    {
        return $this->inscritFaculte;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->id.'-'.$this->nom.'-'.$this->prenom;
    }
}
