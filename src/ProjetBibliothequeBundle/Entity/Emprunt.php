<?php

namespace ProjetBibliothequeBundle\Entity;

/**
 * Emprunt
 */
class Emprunt
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateEmprunt;

    /**
     * @var \ProjetBibliothequeBundle\Entity\Exemplaire
     */
    private $empruntsExemplaire;

    /**
     * @var \ProjetBibliothequeBundle\Entity\Inscrit
     */
    private $empruntsInscrit;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateEmprunt
     *
     * @param \DateTime $dateEmprunt
     *
     * @return Emprunt
     */
    public function setDateEmprunt($dateEmprunt)
    {
        $this->dateEmprunt = $dateEmprunt;

        return $this;
    }

    /**
     * Get dateEmprunt
     *
     * @return \DateTime
     */
    public function getDateEmprunt()
    {
        return $this->dateEmprunt;
    }

    /**
     * Set empruntsExemplaire
     *
     * @param \ProjetBibliothequeBundle\Entity\Exemplaire $empruntsExemplaire
     *
     * @return Emprunt
     */
    public function setEmpruntsExemplaire(\ProjetBibliothequeBundle\Entity\Exemplaire $empruntsExemplaire = null)
    {
        $this->empruntsExemplaire = $empruntsExemplaire;

        return $this;
    }

    /**
     * Get empruntsExemplaire
     *
     * @return \ProjetBibliothequeBundle\Entity\Exemplaire
     */
    public function getEmpruntsExemplaire()
    {
        return $this->empruntsExemplaire;
    }

    /**
     * Set empruntsInscrit
     *
     * @param \ProjetBibliothequeBundle\Entity\Inscrit $empruntsInscrit
     *
     * @return Emprunt
     */
    public function setEmpruntsInscrit(\ProjetBibliothequeBundle\Entity\Inscrit $empruntsInscrit = null)
    {
        $this->empruntsInscrit = $empruntsInscrit;

        return $this;
    }

    /**
     * Get empruntsInscrit
     *
     * @return \ProjetBibliothequeBundle\Entity\Inscrit
     */
    public function getEmpruntsInscrit()
    {
        return $this->empruntsInscrit;
    }
}
