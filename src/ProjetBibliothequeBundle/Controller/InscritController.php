<?php

namespace ProjetBibliothequeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ProjetBibliothequeBundle\Entity\Inscrit;
use ProjetBibliothequeBundle\Form\InscritType;

/**
 * Inscrit controller.
 *
 */
class InscritController extends Controller
{
    /**
     * Lists all Inscrit entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $inscrits = $em->getRepository('ProjetBibliothequeBundle:Inscrit')->findAll();

        return $this->render('ProjetBibliothequeBundle:Inscrit:index.html.twig', array(
            'inscrits' => $inscrits,
        ));
    }

    /**
     * Creates a new Inscrit entity.
     *
     */
    public function newAction(Request $request)
    {
        $inscrit = new Inscrit();
        $form = $this->createForm('ProjetBibliothequeBundle\Form\InscritType', $inscrit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($inscrit);
            $em->flush();

            return $this->redirectToRoute('inscrit_show', array('id' => $inscrit->getId()));
        }

        return $this->render('ProjetBibliothequeBundle:Inscrit:new.html.twig', array(
            'inscrit' => $inscrit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Inscrit entity.
     *
     */
    public function showAction(Inscrit $inscrit)
    {
        $deleteForm = $this->createDeleteForm($inscrit);

        return $this->render('ProjetBibliothequeBundle:Inscrit:show.html.twig', array(
            'inscrit' => $inscrit,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Inscrit entity.
     *
     */
    public function editAction(Request $request, Inscrit $inscrit)
    {
        $deleteForm = $this->createDeleteForm($inscrit);
        $editForm = $this->createForm('ProjetBibliothequeBundle\Form\InscritType', $inscrit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($inscrit);
            $em->flush();

            return $this->redirectToRoute('inscrit_edit', array('id' => $inscrit->getId()));
        }

        return $this->render('ProjetBibliothequeBundle:Inscrit:edit.html.twig', array(
            'inscrit' => $inscrit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Inscrit entity.
     *
     */
    public function deleteAction(Request $request, Inscrit $inscrit)
    {
        $form = $this->createDeleteForm($inscrit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($inscrit);
            $em->flush();
        }

        return $this->redirectToRoute('inscrit_index');
    }

    /**
     * Creates a form to delete a Inscrit entity.
     *
     * @param Inscrit $inscrit The Inscrit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Inscrit $inscrit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('inscrit_delete', array('id' => $inscrit->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
