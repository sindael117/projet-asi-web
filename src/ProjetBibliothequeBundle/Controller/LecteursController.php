<?php

namespace ProjetBibliothequeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ProjetBibliothequeBundle\Entity\Inscrit;
use ProjetBibliothequeBundle\Entity\Emprunt;
use ProjetBibliothequeBundle\Entity\Livre;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class LecteursController extends Controller
{
    public function indexAction()
    {
        return $this->render('ProjetBibliothequeBundle:Lecteurs:index.html.twig');
    }

    public function rechercheNomAction(Request $request)
    {
       $inscrit = new inscrit;
        $form = $this->createFormBuilder($inscrit)
                        ->add('nom', TextType::class)
                        ->add('rechercher', SubmitType::class)
                        ->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) 
        {
            return $this->redirectToRoute('projet_bibliotheque_lecteurs_resultatNom', array('id' => $inscrit->getNom() ));
        }

    	return $this->render('ProjetBibliothequeBundle:Lecteurs:rechercheNom.html.twig', array('form' => $form->createView() ));
    }


    public function resultatNomAction($id)
    {
            $entityManager = $this->getDoctrine()->getManager();
            $repositoryInscrit = $entityManager->getRepository('ProjetBibliothequeBundle:Inscrit');
            $inscrits = $repositoryInscrit->findAll();
            $results = array();
            foreach ($inscrits as $inscrit) 
            {
                if(strpos(strtolower($inscrit->getNom()),$id) != null)
                    array_push($results,$inscrit);
            }
    	return $this->render('ProjetBibliothequeBundle:Lecteurs:resultatNom.html.twig', array('results' => $results ));
    }

    public function pretAction(Inscrit $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryEmprunt = $entityManager->getRepository('ProjetBibliothequeBundle:Emprunt');
        $results= $repositoryEmprunt->getAllEmpruntPourUnInscrit($id);
        return $this->render('ProjetBibliothequeBundle:Lecteurs:pret.html.twig', array('results' => $results));
    }

    public function rechercheLivreAction(Request $request)
    {
        $entityManager=$this->getDoctrine()->getManager();
        $repository=$entityManager->getRepository('ProjetBibliothequeBundle:Theme');
        $themes = $repository->getThemes();
         
         return $this->render('ProjetBibliothequeBundle:Lecteurs:rechercheLivre.html.twig',array('themes'=>$themes));
    }

    public function resultatLivreAction($theme)
    {
        $entityManager=$this->getDoctrine()->getManager();
        $repository=$entityManager->getRepository('ProjetBibliothequeBundle:Livre');
        $livres = $repository->findOneByLivreThemes($theme);
         
         return $this->render('ProjetBibliothequeBundle:Lecteurs:resultatLivre.html.twig',array('livres'=>$livres));
    }





}
    


