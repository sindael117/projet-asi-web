<?php

namespace ProjetBibliothequeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ProjetBibliothequeBundle\Form\ExemplaireType;
use ProjetBibliothequeBundle\Form\TitreType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use ProjetBibliothequeBundle\Entity\Livre;
use ProjetBibliothequeBundle\Entity\Auteur;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ConservateurController extends Controller
{
    public function indexAction()
    {
        return $this->render('ProjetBibliothequeBundle:Conservateur:index.html.twig');
    }

    public function afficheLivreAction()
    {
        return $this->render('ProjetBibliothequeBundle:Conservateur:livres.html.twig');
    }

    public function rechercheAction(Request $request)
    {
        $livre=new Livre();
        $auteur=new Auteur();

        $form=$this->createFormBuilder($livre)
            ->add('titre',TextType::class)
            ->add('Rechercher',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if($form->isValid()){
            $entityManager=$this->getDoctrine()->getManager();
            $repository=$entityManager->getRepository('ProjetBibliothequeBundle:Livre');
            $tmp=$livre->getTitre();
            $livres=$repository->listerLivres($tmp);
            $repository=$entityManager->getRepository('ProjetBibliothequeBundle:Exemplaire');
            foreach ($livres as $unLivre){
                $nbDispo=$unLivre['nbExemplaire']-$repository->countByIdExemplaire($unLivre['id']);
                $unLivre['nbExemplaire']=$nbDispo;
            }
            return $this->render('ProjetBibliothequeBundle:Conservateur:resultat.html.twig', array('livres' => $livres));
        }
        return $this->render('ProjetBibliothequeBundle:Conservateur:recherche.html.twig',array('form'=>$form->createView()));

    }
}

?>